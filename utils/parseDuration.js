function fillNumber(numStr) {
    const n = Number(numStr) || 0;
    if (n < 10)
        return '0' + n;
    else
        return n;
};

function parseDuration(duration) {
    const durationReg = /^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?$/;
    const matches = durationReg.exec(duration);
    let [_, h, m, s] = matches;

    m = fillNumber(m);
    s = fillNumber(s);

    if (h)
        return `${h}:${m}:${s}`;
    else
        return `${m}:${s}`;
};

export default parseDuration;