import getConfig from 'next/config';
import axios from 'axios';
import store from 'store';

const { publicRuntimeConfig } = getConfig()

const fetchVideo = async videoId => {

    const video = await axios(`https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails&id=${videoId}&regionCode=TW&key=${publicRuntimeConfig.apiKey}`)
        .then(res => res.data.items[0])
        .then(item => ({
            id: item.id,
            title: item.snippet.title,
            description: item.snippet.description,
            thumbnailUrl: item.snippet.thumbnails.medium.url,
            duration: item.contentDetails.duration,
        }))
        .catch(() => null);

    return video;
};

export default fetchVideo;