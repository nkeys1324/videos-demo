import getConfig from 'next/config';
import axios from 'axios';
import store from 'store';
import parseDuration from '@utils/parseDuration';

const { publicRuntimeConfig } = getConfig()

const fetchPopularVideos = async () => {

    const result1 = await axios(`https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails&chart=mostPopular&maxResults=100&regionCode=TW&key=${publicRuntimeConfig.apiKey}`)
        .then(res => res.data)
        .catch(() => null);

    const result2 = await axios(`https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails&chart=mostPopular&maxResults=100&regionCode=TW&pageToken=${result1?.nextPageToken}&key=${publicRuntimeConfig.apiKey}`)
        .then(res => res.data)
        .catch(() => null);

    console.log(result1, result2)

    const items = [...result1?.items, ...result2?.items];

    const likedVideoIds = store.get('likedVideoIds') || {};

    return items?.map(item => ({
        id: item.id,
        title: item.snippet.title,
        description: item.snippet.description,
        thumbnailUrl: item.snippet.thumbnails.medium.url,
        duration: parseDuration(item.contentDetails.duration),
        liked: Boolean(likedVideoIds[item.id])
    }));
};

export default fetchPopularVideos;