import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import Layout from '@components/Layout';
import fetchVideo from '@services/fetchVideo';

function Video() {
    const router = useRouter();
    const [video, setVideo] = useState();
    const videoId = router.query.video_id;

    useEffect(() => {
        if (videoId)
            (async () => {
                const video = await fetchVideo(videoId);
                setVideo(video);
            })();
    }, [videoId])

    return (
        <Layout>
            <div className='root'>
                {videoId && (
                    <iframe width="560" height="315"
                        src={`https://www.youtube.com/embed/${videoId}`}
                        frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>)}
                <h2>{video?.title}</h2>
                <pre>{video?.description}</pre>
                <style jsx>{`
                    .root {
                        padding: 16px 16px 64px 16px;
                        max-width: 600px
                    }
                    h2 {
                        font-size: 1.2rem;
                        margin: 16px 0;
                    }
                `}</style>
            </div>
        </Layout>
    )
};

export default Video;